#pragma once
#include <SFML\Graphics.hpp>
#include "RigidBody.h"
#include "Networking.h"
#include "Common.h"

class NetworkPlayer
{
public:
	~NetworkPlayer();
	NetworkPlayer(sf::Vector2f* position, sf::Vector2f size, sf::Color color);
	RigidBody* rigidBody;
	sf::Texture* texture;
	sf::Sprite* sprite;
	sf::Vector2i* actualSprite;
	sf::Vector2f* position;
	void update(sf::Vector2f position);
	void render(sf::RenderWindow* window);
	float speed;
};

