#pragma once
#include<SFML/Graphics.hpp>

class RigidBody
{
public:
	sf::RectangleShape rect;
	float bottom, left, right, top;
	RigidBody(sf::Vector2f position, sf::Vector2f size, sf::Color color);
	~RigidBody();
	bool Collision(RigidBody rb);
	void update();
};

