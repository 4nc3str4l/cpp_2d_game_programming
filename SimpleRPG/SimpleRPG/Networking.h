#pragma once
#include<SFML/Network.hpp>


class Networking
{
public:
	Networking(sf::IpAddress* ip, int port);
	void update();
	sf::TcpSocket* socket;
	sf::Packet* packet, *recPacket;
	bool send();
	bool recieve();
	int id;
	bool socket_in_use;
	~Networking();
private:
	sf::IpAddress* ip;
	std::size_t received;
	char buffer[2000];
};
