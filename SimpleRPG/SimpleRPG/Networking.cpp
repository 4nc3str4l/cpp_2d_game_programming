#include "Networking.h"
#include<iostream>

Networking::Networking(sf::IpAddress* ip, int port)
{
	this->socket_in_use = false;
	this->ip = ip;
	this->socket = new sf::TcpSocket();
	this->socket->connect(sf::IpAddress::getLocalAddress(), 2000, sf::Time::Zero);
	packet = new sf::Packet();
	recPacket = new sf::Packet();
	this->socket->setBlocking(false);
	*packet << 0x01;
	this->send();

}

bool Networking::send()
{
	//mutex?
	if (socket_in_use) 
		return false;
	
	socket_in_use = true;
	socket->send(*packet);
	socket_in_use = false;
	packet->clear();
	return true;

}

bool Networking::recieve()
{
	this->recPacket->clear();
	sf::IpAddress senderIP;
	unsigned short port;

	//mutex?
	if (socket_in_use)
		return false;
	

	std::size_t received = 0;
	socket->receive(buffer, sizeof(buffer), received);
	
	std::cout << "Server sent:" << buffer << std::endl;

	int i = (buffer[3] << 24) | (buffer[2] << 16) | (buffer[1] << 8) | (buffer[0]);

	std::cout << i << std::endl;

	system("pause");

	return true;
}

Networking::~Networking()
{
}

void Networking::update()
{

}