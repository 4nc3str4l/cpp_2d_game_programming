#pragma once
#include <SFML\Graphics.hpp>

class VisibleEntity
{
public:
	VisibleEntity(sf::Texture texture);
	~VisibleEntity();
	void setVisible(bool visible);
	bool isVisible();
	void render();
	void setTexture(sf::IntRect actualSprite);
	void render(sf::RenderWindow window);
	sf::Texture texture;
	sf::Sprite sprite;
	bool visible;
};

