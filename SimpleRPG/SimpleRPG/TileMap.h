#pragma once
#include<iostream>
#include <SFML\Graphics.hpp>
#include "Common.h"

class TileMap
{
public:
	TileMap(int width, int height, int tileWidth, int tileHeight);
	void render(sf::RenderWindow* window, sf::Vector2f* playerPos);
	~TileMap();
	void load(std::string path);
private:
	int width, height, tileWidth, tileHeight;
	sf::Texture *grass, *water, *sand;
	std::vector<sf::Sprite*>* tiles;
	sf::Texture* texture;
};

