#include "Game.h"
#include "TileMap.h"
#include <Windows.h>


#include<iostream>

std::string TITLE = "SIMPLE MMORG";
sf::Vector2i screenDimensions(800, 600);
sf::RenderWindow window(sf::VideoMode(screenDimensions.x, screenDimensions.y), TITLE);
const sf::Time TimePerFrame = sf::seconds(1.f / 60.f);
sf::View view;


//Game constructor
Game::Game()
{
	if (ONLINE)
		this->network = new Networking(&sf::IpAddress::getLocalAddress(), 2224);

	if (!font.loadFromFile("Royal.ttf"))
		std::cout << "Could not open royal font" << std::endl;

	this->network->socket->setBlocking(true);
	this->network->recieve();
	*this->network->recPacket >> id;
	this->network->id = this->id;
	this->network->socket->setBlocking(false);
	this->network->recPacket->clear();

	std::cout << "This is my new id " << this->id <<  std::endl;

	this->player = new Player(&sf::Vector2f(100, 100), sf::Vector2f(32, 32), sf::Color::Red);
	ups = 0;
	tilemap = new TileMap(3200, 3200, 16, 16);
	tilemap->load("map.bmp");

	this->thread = new std::thread(&Game::tickNetwork, *this);
}


void updateWindow()
{
	// Process events
	sf::Event event;
	while (window.pollEvent(event))
	{
		// Close window: exit
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

void Game::tick()
{
	if (!window.hasFocus())
		return;
	if (ups > 60)
		ups = 0;

	updateWindow();
	this->player->update(ups, this->network);
	//std::cout << player->position->x << ", " << player->position->y << std::endl;
	++ups;
}

void Game::tickNetwork()
{
	int header;
	int thing_id;
	while (true)
	{
		this->network->recieve();
		*this->network->recPacket >> header;
		switch (header)
		{
			case NEW_PLAYER:
				std::cout << "New Player Connected" << std::endl;
				break;
			case PLAYER_POS:
				float x, y;
				*this->network->recPacket >> thing_id >> x >> y;
				std::cout << "Player id: " << thing_id << "Player Position" << "(" << x << ", " << y << ")" << std::endl;
				break;
			case PLAYER_DISCONECTED:
				std::cout << "Player Disconected" << std::endl;
				break;
			default:
				break;
		}
		header = -1;
		this->network->recPacket->clear();
		Sleep(3);
	}
}

void Game::render()
{
	view.setCenter(sf::Vector2f(player->position->x + 16, player->position->y + 16));

	window.setView(view);
	
	this->tilemap->render(&window, this->player->position);
	this->player->render(&window);
	window.setView(window.getDefaultView());
	window.display();
	window.clear();
}

int Game::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	view.reset(sf::FloatRect(0, 0, screenDimensions.x , screenDimensions.y));
	view.setViewport(sf::FloatRect(0, 0, 1, 1));

	while (window.isOpen())
	{
		sf::Time elapsedTime = clock.restart();
		timeSinceLastUpdate += elapsedTime;

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			tick();
		}

		render();
	}

	*this->network->packet << 0x03;
	this->network->send();
	return EXIT_SUCCESS;
}


Game::~Game()
{
}

