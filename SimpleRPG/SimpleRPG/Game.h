#pragma once
#include<string>
#include<thread>

#include "Player.h";
#include "Networking.h"
#include "TileMap.h"
#include "Common.h"

class Game
{
public:
	Game();
	~Game();
	int run();
private:
	TileMap* tilemap;
	sf::Font font;
	sf::Text mainMenuTitle;
	Player* player;
	Networking* network;
	std::thread* thread;

	int ups, id;
	void renderMenu();
	void tick();
	void render();
	void tickNetwork();
};

