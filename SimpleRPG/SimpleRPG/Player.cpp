#include "Player.h"
#include <iostream>

#define PLAYER_SPRITE_SIZE 32
#define NUM_SPRITES 3

bool moving = false;
int ups_to_transition = 10;

Player::Player(sf::Vector2f* position, sf::Vector2f size, sf::Color color)
{
	this->rigidBody = new RigidBody(*position, size, color);
	this->texture = new sf::Texture();
	if (!this->texture->loadFromFile("Player.png"))
		std::cout << "Could not load Player.png" << std::endl;
	this->sprite = new sf::Sprite();
	this->sprite->setTexture(*this->texture);
	this->actualSprite = new sf::Vector2i(1, Down);
	this->position = new sf::Vector2f(0, 0);
	this->speed = 1.0f;
}

void Player::update(int ups, Networking* network)
{
	//Update the bounding box
	this->rigidBody->update();

	moving = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
	{
		speed = 4.0f;
		ups_to_transition = 5;
	}
	else
	{
		speed = 1.0f;
		ups_to_transition = 10;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		this->actualSprite->y = Up;
		if (this->position->y - 1 * speed > 0)
		{
			this->sprite->move(0, -1 * speed);
			this->position->y -= 1 * speed;
		}
		moving = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		this->actualSprite->y = Down;
		this->sprite->move(0, 1 * speed);
		this->position->y += 1 * speed;
		moving = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		this->actualSprite->y = Right;
		this->sprite->move(1 * speed, 0);
		this->position->x += 1 * speed;
		moving = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		this->actualSprite->y = Left;
		if (this->position->x - 1 * speed - 12 > 0)
		{
			this->sprite->move(-1 * speed, 0);
			this->position->x -= 1 * speed;
		}
		moving = true;
	}

	if (moving && ups % ups_to_transition == 0)
	{
		this->actualSprite->x++;
		if (this->actualSprite->x == NUM_SPRITES)
			this->actualSprite->x = 0;
	}

	this->sprite->setTextureRect(sf::IntRect(this->actualSprite->x * PLAYER_SPRITE_SIZE, this->actualSprite->y * PLAYER_SPRITE_SIZE, PLAYER_SPRITE_SIZE, PLAYER_SPRITE_SIZE));

	if (moving)
	{
		*network->packet << 0x02 << network->id << this->position->x << this->position->y;
		
		bool sent = false;
		while (!sent){
			sent = network->send();
		}
	}
}

void Player::render(sf::RenderWindow* window)
{
	window->draw(*this->sprite);
}

Player::~Player()
{
}
