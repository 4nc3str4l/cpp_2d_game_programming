#include "VisibleEntity.h"



VisibleEntity::VisibleEntity(sf::Texture texture)
{
	this->sprite = sf::Sprite();
	this->texture = texture;
	this->visible = true;
	this->sprite.setTexture(texture);
}

void VisibleEntity::setVisible(bool visible)
{
	this->visible = visible;
}

bool VisibleEntity::isVisible()
{
	return this->visible;
}

void VisibleEntity::setTexture(sf::IntRect actualSprite){
	this->sprite.setTextureRect(actualSprite);
}

void VisibleEntity::render(sf::RenderWindow window)
{
	if (this->visible) window.draw(this->sprite);
}

VisibleEntity::~VisibleEntity()
{
}
