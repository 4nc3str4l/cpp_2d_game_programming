#include "RigidBody.h"


RigidBody::RigidBody(sf::Vector2f position, sf::Vector2f size, sf::Color color)
{
	rect.setPosition(position);
	rect.setSize(size);
	rect.setFillColor(color);
}


RigidBody::~RigidBody()
{
}

bool RigidBody::Collision(RigidBody rb)
{
	if (right < rb.left || left > rb.right || top > rb.bottom || bottom < rb.top)
		return false;
	return true;
}

void RigidBody::update(){
	bottom = rect.getPosition().y + rect.getSize().y;
	left = rect.getPosition().x;
	right = rect.getPosition().x + rect.getSize().x;
	top = rect.getPosition().y;
}

