#include "TileMap.h"
#include "bitmap_image.hpp"


TileMap::TileMap(int width, int height, int tileWidth, int )
{
	this->width = width;
	this->height = height;
	this->tileWidth = tileWidth;
	this->tileHeight = tileHeight;
	this->tiles = new std::vector<sf::Sprite*>();

	this->grass = new sf::Texture();
	this->grass->loadFromFile("tilemap.png", sf::IntRect(0, 16, 16, 16));

	this->sand = new sf::Texture();
	this->sand->loadFromFile("tilemap.png", sf::IntRect(0, 32, 16, 16));

	water = new sf::Texture();
	water->loadFromFile("tilemap.png", sf::IntRect(0, 0, 16, 16));
}

void TileMap::load(std::string path)
{
	bitmap_image image(path);

	if (!image)
		std::cout << "Could not load map" << std::endl;

	unsigned char red;
	unsigned char green;
	unsigned char blue;

	unsigned int total_number_of_pixels = 0;

	std::cout << "map size" << this->width << ", " << this->height << std::endl;

	for (std::size_t y = 0; y < this->height; ++y)
	{
		for (std::size_t x = 0; x < this->width; ++x)
		{
			image.get_pixel(x, y, red, green, blue);
			if (red == 247 && green == 255 && blue == 99)
				this->tiles->push_back(new sf::Sprite(*this->sand));
			else if (red == 36 && green == 124 && blue == 12)
				this->tiles->push_back(new sf::Sprite(*this->grass));
			else if (red == 0 && green == 148 && blue == 255)
				this->tiles->push_back(new sf::Sprite(*this->water));
			else
				this->tiles->push_back(new sf::Sprite(*this->sand));
		}
	}
		
}

void TileMap::render(sf::RenderWindow* window, sf::Vector2f* playerPos)
{

	for (int i = round((playerPos->y - 400) / 16); i < round((playerPos->y + 400) / 16); i++)
	{
		for (int j = round((playerPos->x - 450) / 16); j < round((playerPos->x + 450) / 16); j++)
		{
			if (i > 0 && j > 0)
			{
				this->tiles->at(j + i * this->width)->setPosition(j * 16, i * 16);
				window->draw(*this->tiles->at(j + i * this->width));
			}
		}
	}


}

TileMap::~TileMap()
{

}
