#pragma once

#include "RigidBody.h"
#include "Common.h"
#include "Networking.h"

class Player 
{
public:

	Player(sf::Vector2f* position, sf::Vector2f size, sf::Color color);
	RigidBody* rigidBody;
	sf::Texture* texture;
	sf::Sprite* sprite;
	sf::Vector2i* actualSprite;
	sf::Vector2f* position;
	void update(int ups, Networking* network);
	void render(sf::RenderWindow* window);
	float speed;
	~Player();

};