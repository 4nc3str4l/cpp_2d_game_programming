#include "NetworkPlayer.h"

#include <iostream>

#define PLAYER_SPRITE_SIZE 32
#define NUM_SPRITES 3

bool moving = false;
int ups_to_transition = 10;

NetworkPlayer::NetworkPlayer(sf::Vector2f* position, sf::Vector2f size, sf::Color color)
{
	this->rigidBody = new RigidBody(*position, size, color);
	this->texture = new sf::Texture();
	if (!this->texture->loadFromFile("Player.png"))
		std::cout << "Could not load Player.png" << std::endl;
	this->sprite = new sf::Sprite();
	this->sprite->setTexture(*this->texture);
	this->actualSprite = new sf::Vector2i(1, Down);
	this->position = new sf::Vector2f(0, 0);
	this->speed = 1.0f;
}

void NetworkPlayer::update(sf::Vector2f position)
{
	this->sprite->setPosition(position);
}

void NetworkPlayer::render(sf::RenderWindow* window)
{
	window->draw(*this->sprite);
}

NetworkPlayer::~NetworkPlayer()
{
}
