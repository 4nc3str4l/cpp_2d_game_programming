#include<SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include<SFML/Network.hpp>
#include<iostream>
#include<string>
#include<map>
#include<conio.h>

using namespace std;

int main()
{
	sf::IpAddress ip = sf::IpAddress::getLocalAddress();

	sf::UdpSocket socket;
	char connectionType, mode;
	char buffer[2000];
	std::size_t received;
	std::map<unsigned short, sf::IpAddress> computerID;
	std::string  text = "Conected to; ";

	//std::cout << ip << std::endl;

	std::cout << "Enter (s) for Server, Enter(c) for client:" << std::endl;
	cin >> connectionType;

	unsigned short port;

	std::cout << "Set port number: ";
	cin >> port;

	socket.bind(port);

	//socket.setBlocking(false);

	if (connectionType == 's')
	{
		char answer = 'b';
		do
		{
			sf::IpAddress rIp;
			unsigned short port;
			socket.receive(buffer, sizeof(buffer), received, rIp, port);
			if (received > 0)
				computerID[port] = rIp;
			cin >> answer;
		} while (answer != 'a');
	}
	else
	{
		std::string sIP;
		std::cout << "Enter server ip ";
		char c;
		while (true)
		{
			c = (char)_getch();
			if (c == 13)
				break;
			sIP += c;
			std::cout << "*";
		}
		socket.send(text.c_str(), text.length() + 1, sf::IpAddress(sf::IpAddress::getLocalAddress()), 2000);
	}

	bool done = false;

	while (!done)
	{
		if (connectionType == 's')
		{
			std::getline(cin, text);
			std::map<unsigned short, sf::IpAddress>::iterator tempIterator;
			for (tempIterator = computerID.begin(); tempIterator != computerID.end(); tempIterator++)
				socket.send(text.c_str(), text.length() + 1, tempIterator->second, tempIterator->first);
		}
		else
		{
			sf::IpAddress tempIp;
			unsigned short tempPort;
			socket.receive(buffer, sizeof(buffer), received, tempIp, tempPort);
			if (received > 0)
				std::cout << "Received: " << buffer << std::endl;
		}
	}

	system("pause");

	return 0;

}
