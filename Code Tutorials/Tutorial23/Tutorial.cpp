#include<SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include<SFML/Network.hpp>
#include<iostream>
#include<string>

using namespace std;

int main()
{
	sf::IpAddress ip = sf::IpAddress::getLocalAddress();
	sf::TcpSocket socket;
	char connectionType, mode;
	char buffer[2000];
	std::size_t recieved;
	std::string text = "Conected to: ";

	cout << "Enter (s) for server, Enter (c) for client" << std::endl;
	cin >> connectionType;

	if (connectionType == 's')
	{
		sf::TcpListener listener;
		listener.listen(2000);
		listener.accept(socket);
		text += "Server";
		mode = 's';
	}
	else if (connectionType == 'c')
	{
		socket.connect(ip, 2000);
		text += "Client";
		mode = 'r';
	}
	socket.send(text.c_str(), text.length() + 1);
	socket.receive(buffer, sizeof(buffer), recieved);

	std::cout << buffer << std::endl;

	bool done = false;

	while (!done)
	{
		if (mode == 's')
		{
			std::getline(cin, text);
			socket.send(text.c_str(), text.length() + 1);
			mode = 'r';
		}
		else if (mode == 'r')
		{
			socket.receive(buffer, sizeof(buffer), recieved);
			if (recieved > 0)
			{
				std::cout << "Recieved: " << buffer << std::endl;
				mode = 's';
			}
		}
	}

	system("pause");
}
