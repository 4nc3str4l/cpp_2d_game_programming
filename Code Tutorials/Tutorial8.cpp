//Text events
#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>

int main()
{
	sf::RenderWindow Window;
	Window.create(sf::VideoMode(800, 600), "Learning SFML");
	int index = 0;
	std::string display;

	Window.setKeyRepeatEnabled(false);

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::TextEntered:
				if (Event.text.unicode > 31 && Event.text.unicode <= 126)
				{
					display +=  (char)Event.text.unicode;
				}
				else if (Event.text.unicode == 8)
					display = display.substr(0, display.length() - 1);
				system("cls");
				std::cout << display;
				break;

			}
		}
		Window.display();
	}
}
