#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>


int main()
{
	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow Window;
	Window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Learning SFML");

	sf::Texture bTexture;
	sf::Sprite bImage;

	if (!bTexture.loadFromFile("Background.png"))
		std::cout << "Error could not load player image" << std::endl;

	bImage.setTexture(bTexture);
	bImage.setScale(1.0f, (float)screnDimensions / bTexture.getSize().y);

	sf::RectangleShape rect(sf::Vector2f(20, 20));
	rect.setFillColor(sf::Color::Red);

	sf::Clock clock;

	float moveSpeed = 10000.0f;

	Window.setKeyRepeatEnabled(false);

	while (Window.isOpen())
	{
		clock.restart();
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
					Window.close();
				break;
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::KeyBoard::Right))
			rect.move(moveSpeed * clock.getElapsedTime().asSeconds(), 0);
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			rect.move(-moveSpeed * clock.getElapsedTime().asSeconds(), 0);
		
		Window draw(bImage);
		Window.display();
		Window.clear();
	}
}
