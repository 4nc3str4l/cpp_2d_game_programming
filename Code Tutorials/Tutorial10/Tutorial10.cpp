//Famitzu generator
//Text events
#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>

int main()
{
	sf::RenderWindow Window;
	Window.create(sf::VideoMode(800, 600), "Learning SFML");
	int index = 0;
	std::string display;

	Window.setKeyRepeatEnabled(false);

	sf::Texture pTexture;
	sf::Sprite playerImage;

	if (!pTexture.loadFromFile("Player.png", sf::IntRect(32, 0 , 32, 32)))
		std::cout << "Error could not load player image" << std::endl;

	playerImage.setTexture(pTexture);

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			}
		}
		Window.draw(playerImage);
		Window.display();
	}
}
