#include<SFML/Graphics.hpp>
#include<iostream>

class Player
{
public:
	sf::RectangleShape rect;
	float bottom, left, right, top;

	Player(sf::Vector2f position, sf::Vector2f size, sf::Color color)
	{
		rect.setPosition(position);
		rect.setSize(size);
		rect.setFillColor(color);
	}

	void Update()
	{
		bottom = rect.getPosition().y + rect.getSize().y;
		left = rect.getPosition().x;
		right = rect.getPosition().x + rect.getSize().x;
		top = rect.getPosition().y;
	}

	bool Collision(Player p)
	{
		if (right < p.left || left > p.right || top > p.bottom || bottom < p.top)
			return false;
		return true;
	}


};

int main()
{
	sf::RenderWindow Window(sf::VideoMode(680, 480, 32), "Bounding box Collision");

	Player p1(Player(sf::Vector2f(10, 10), sf::Vector2f(20, 20), sf::Color(sf::Color::Red)));
	Player p2(Player(sf::Vector2f(100, 100), sf::Vector2f(20, 20), sf::Color(sf::Color::Blue)));

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			p1.rect.move(1.0f, 0);
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			p1.rect.move(-1.0f, 0);
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			p1.rect.move(0, -1.0f);
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			p1.rect.move(0, 1.0f);

		p1.Update();
		p2.Update();

		if (p1.Collision(p2))
			std::cout << "Collision!" << std::endl;
		Window.clear(sf::Color(0, 240, 255));
		Window.draw(p1.rect);
		Window.draw(p2.rect);
		Window.display();
	}
}
