#include<SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include<iostream>
#include<string>


int main()
{
	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow Window;
	Window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Learning SFML");


	sf::SoundBuffer soundBuffer;
	sf::Sound sound;

	sf::Music music;

	if (!soundBuffer.loadFromFile("beep.wav"))
		std::cout << "Could not load sound file" << std::endl;

	if (!music.openFromFile("bg.ogg"))
		std::cout << "Could not load sound from file" << std::endl;

	sound.setBuffer(soundBuffer);
	sf::Clock clock;

	float moveSpeed = 10000.0f;
	sound.setLoop(true);

	while (Window.isOpen())
	{
		clock.restart();
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
					Window.close();
				if (Event.key.code == sf::Keyboard::P)
					music.play();
				break;
			}
		}
		Window.display();
		Window.clear();
	}
}
