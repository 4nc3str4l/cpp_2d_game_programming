//Text events
#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>

int main()
{
	enum Direction { Down, Left, Right, Up };

	sf::Vector2i source(1, Down);

	float frameCounter = 0, switchFrame = 100, frameSpeed = 500;

	sf::RenderWindow Window;
	Window.create(sf::VideoMode(800, 600), "Learning SFML");
	int index = 0;
	std::string display;

	Window.setKeyRepeatEnabled(false);

	sf::Texture pTexture;
	sf::Sprite playerImage;

	bool updateFrame = true;

	sf::Clock clock;

	if (!pTexture.loadFromFile("Player.png"))
		std::cout << "Error could not load player image" << std::endl;

	playerImage.setTexture(pTexture);

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
					Window.close();
				break;
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			source.y = Up;
			playerImage.move(0, -1);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			source.y = Down;
			playerImage.move(0, 1);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			source.y = Right;
			playerImage.move(1, 0);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			source.y = Left;
			playerImage.move(-1, 0);
		}

		sf::Vector2i mousePosition = sf::Mouse::getPosition(Window);
		//Set the mouse position to a site
		//sf::Mouse::setPosition(sf::Vector2i(100, 100), Window);

		std::cout << "(" << mousePosition.x << " , " << mousePosition.y << ")" << std::endl;

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			updateFrame = true;
		else if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
			updateFrame = false;

		frameCounter = (updateFrame) ? frameCounter + frameSpeed * clock.restart().asSeconds() : 0;

		if (frameCounter >= switchFrame)
		{
			frameCounter = 0;
			source.x++;
			if (source.x * 32 >= pTexture.getSize().x)
				source.x = 0;
		}



		playerImage.setTextureRect(sf::IntRect(source.x * 32, source.y * 32, 32, 32));
		Window.draw(playerImage);
		Window.display();
		Window.clear();
	}
}
