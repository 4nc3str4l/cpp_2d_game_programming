#include<SFML/Graphics.hpp>
#include<string>
#include<iostream>

int main()
{
	sf::RenderWindow Window;
	Window.create(sf::VideoMode(800, 600), "Tutorial 5", sf::Style::Titlebar | sf::Style::Close);

	std::string message = "Hello my name is cristian";
	std::string display = "";

	int index = 0;

	//disable having repeated keys on the event screen
	Window.setKeyRepeatEnabled(false);

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			if (Event.type == sf::Event::Closed)
				Window.close();

			if (Event.type == sf::Event::KeyPressed)
			{
				if (Event.key.code == sf::Keyboard::Return)
				{
					display += message[index];
					index++;
					system("cls");
					std::cout << display;
				}
			}
		}
		Window.display();
	}
}