#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>

using namespace std;
int main()
{
	sf::RenderWindow Window(sf::VideoMode(800, 600), "test 2", sf::Style::Titlebar | sf::Style::Close);

	std::cout << "Press a key to continue " << endl;
	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			if (Event.type == sf::Event::Closed)
				Window.close();
		}

		if (Window.waitEvent(Event)){
			cout << "Event Activated" << endl;
		}

		Window.display();
	}
}