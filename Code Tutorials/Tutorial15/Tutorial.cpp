//Text events
#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>

int main()
{

	sf::RenderWindow Window;
	Window.create(sf::VideoMode(800, 600), "Learning SFML");



	sf::Texture pTexture;
	sf::Sprite playerImage;

	if (!pTexture.loadFromFile("Player.png"))
		std::cout << "Error could not load player image" << std::endl;

	playerImage.setTexture(pTexture);

	/*
	sf::RectangleShape rect(sf::Vector2f(200, 100));
	rect.setFillColor(sf::Color(255, 0 , 100, 100));
	rect.setOutlineColor(sf::Color::Yellow);
	rect.setOutlineThickness(2.0f);
	rect.setPosition(100, 100);
	rect.setTexture(&pTexture);
	*/
	sf::ConvexShape rect(3);
	rect.setPoint(0, sf::Vector2f(10, 10));
	rect.setPoint(1, sf::Vector2f(15, 15));
	rect.setPoint(2, sf::Vector2f(20, 10));
	rect.setOutlineColor(sf::Color::Red);

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
					Window.close();
				break;
			}
		}

		Window.draw(rect);
		Window.display();
		Window.clear();
	}
}
