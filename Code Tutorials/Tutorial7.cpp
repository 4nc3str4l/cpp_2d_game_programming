//Window Events
#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>

int main()
{
	sf::RenderWindow Window;
	Window.create(sf::VideoMode(800, 600), "Learning SFML");
	int index = 0;

	Window.setKeyRepeatEnabled(false);

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::GainedFocus:
				std::cout << "Window Active" << std::endl;
				break;
			case sf::Event::LostFocus:
				std::cout << "Window NOT Active" << std::endl;
				break;
			case sf::Event::Resized:
				std::cout << "New height and width (" << Event.size.width << " , " << Event.size.height << ")" << std::endl;
				break;
			}
		}
		Window.display();
	}
}
