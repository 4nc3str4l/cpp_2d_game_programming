#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>
#include<ctime>
#include<cstdlib>

int main()
{
	sf::RenderWindow Window;

	sf::Vector2i blockDimesions(10, 10);
	sf::Vector2i screenDimensions(800, 600);

	Window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Learning SFML");
	sf::Texture pTexture;
	sf::Sprite playerImage;

	if (!pTexture.loadFromFile("Player.png"))
		std::cout << "Error could not load player image" << std::endl;

	playerImage.setTexture(pTexture);

	srand(time(0));

	Window.setKeyRepeatEnabled(false);

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
					Window.close();
				break;
			}

		}


		for (int i = 0; i < screenDimensions.x / blockDimesions.x; i++)
		{
			for (int j = 0; j < screenDimensions.y / blockDimesions.y; j++)
			{
				sf::VertexArray vArray(sf::PrimitiveType::LinesStrip, 4);
				vArray[0].position = sf::Vector2f(i * blockDimesions.x, j * blockDimesions.y);
				vArray[1].position = sf::Vector2f(i * blockDimesions.x + blockDimesions.x, j * blockDimesions.y + blockDimesions.y);
				vArray[2].position = sf::Vector2f(i * blockDimesions.x + blockDimesions.x,
					j * blockDimesions.y);
				vArray[3].position = sf::Vector2f(i * blockDimesions.x , j * blockDimesions.y + blockDimesions.y);

				for (int k = 0; k < 4; k++)
				{
					int red = rand() % 255;
					int green = rand() % 255;
					int blue = rand() % 255;

					vArray[k].color = sf::Color(red, green, blue);
				}

				Window.draw(vArray);
			}
		}

		Window.display();
		Window.clear();
	}
}
