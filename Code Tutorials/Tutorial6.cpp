//Mouse events
#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>

int main()
{
	sf::RenderWindow Window;
	Window.create(sf::VideoMode(800, 600), "Learning SFML", sf::Style::Titlebar | sf::Style::Close);
	int index = 0;

	Window.setKeyRepeatEnabled(false);

	while (Window.isOpen())
	{
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			//mouse entered in the screen
			case sf::Event::MouseEntered:
				std::cout << "Mouse within screen bounds" << std::endl;
				break;
			//mouse out of the screen
			case sf::Event::MouseLeft:
				std::cout << "Mouse NOT within screen bounds" << std::endl;
				break;
			//mouse movement
			case sf::Event::MouseMoved:
				std::cout << "X:" << Event.mouseMove.x << "Y:" << Event.mouseMove.y << std::endl;
				break;
			case sf::Event::MouseButtonPressed:
				if (Event.mouseButton.button == sf::Mouse::Left)
					std::cout << "Left Button Pressed at: (" << Event.mouseButton.x << " , " << Event.mouseButton.y << ")" << std::endl;
				break;
			case sf::Event::MouseWheelMoved:
				std::cout << "delta: " << Event.mouseWheel.delta << std::endl;
			}

		}
		Window.display();
	}
}
