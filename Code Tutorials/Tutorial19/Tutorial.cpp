//Scene split and camera tracking11145631221421631
#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>


int main()
{
	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow Window;
	Window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Learning SFML");

	sf::Texture bTexture;
	sf::Sprite bImage;

	if (!bTexture.loadFromFile("bg.jpg"))
		std::cout << "Error could not load background image" << std::endl;

	bImage.setTexture(bTexture);
	bImage.setScale(1.0f, (float)screenDimensions.y / bTexture.getSize().y);

	sf::RectangleShape rect(sf::Vector2f(20, 20));
	sf::RectangleShape rect2(sf::Vector2f(20, 20));
	rect.setFillColor(sf::Color::Red);
	rect2.setFillColor(sf::Color::Blue);

	sf::View view1, view2;

	view1.setViewport(sf::FloatRect(0, 0, 0.5f, 1.0f));
	//view1.setSize(screenDimensions.x / 2, screenDimensions.y);

	view2.setViewport(sf::FloatRect(0.5f, 0.0, 0.5f, 1.0f));
	//view2.setSize(screenDimensions.x / 2, screenDimensions.y);

	sf::Vector2f position(screenDimensions.x / 2, screenDimensions.y / 2);
	sf::Vector2f position2(screenDimensions.x / 2, screenDimensions.y / 2);

	sf::Clock clock;

	float moveSpeed = 10000.0f;

	while (Window.isOpen())
	{
		clock.restart();
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
					Window.close();
				break;
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			rect.move(moveSpeed * clock.getElapsedTime().asSeconds(), 0);
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			rect.move(-moveSpeed * clock.getElapsedTime().asSeconds(), 0);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			rect2.move(moveSpeed * clock.getElapsedTime().asSeconds(), 0);
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			rect2.move(-moveSpeed * clock.getElapsedTime().asSeconds(), 0);

		if (rect.getPosition().x + 10 >= view1.getSize().x / 2)
			position.x = rect.getPosition().x + 10;
		else
			position.x = view1.getSize().x / 2;

		if (rect2.getPosition().x + 10 >= view2.getSize().x / 2)
			position2.x = rect2.getPosition().x + 10;
		else
			position2.x = view2.getSize().x / 2;

		view1.setCenter(position);
		view2.setCenter(position2);

		Window.setView(view1);
		Window.draw(bImage);
		Window.draw(rect);
		Window.draw(rect2);

		Window.setView(view2);
		Window.draw(bImage);
		Window.draw(rect2);
		Window.draw(rect);

		Window.display();
		Window.clear();
	}
}
