#include<SFML\Graphics.hpp>
#include<iostream>
#include<string>


int main()
{
	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow Window;
	Window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Learning SFML");

	sf::Texture bTexture;
	sf::Sprite bImage;

	if (!bTexture.loadFromFile("bg.jpg"))
		std::cout << "Error could not load background image" << std::endl;

	bImage.setTexture(bTexture);
	bImage.setScale(1.0f, (float)screenDimensions.y / bTexture.getSize().y);

	sf::RectangleShape rect(sf::Vector2f(20, 20));
	rect.setFillColor(sf::Color::Red);

	sf::Clock clock;

	float moveSpeed = 10000.0f;

	sf::View view;
	view.reset(sf::FloatRect(0, 0, screenDimensions.x, screenDimensions.y));
	view.setViewport(sf::FloatRect(0, 0, 0.5f, 1.0f));

	sf::Vector2f position(screenDimensions.x / 2, screenDimensions.y / 2);

	while (Window.isOpen())
	{
		clock.restart();
		sf::Event Event;
		while (Window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				Window.close();
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
					Window.close();
				break;
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			rect.move(moveSpeed * clock.getElapsedTime().asSeconds(), 0);
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			rect.move(-moveSpeed * clock.getElapsedTime().asSeconds(), 0);

		if (rect.getPosition().x + 10 > screenDimensions.x / 2)
			position.x = rect.getPosition().x + 10;
		else
			position.x = screenDimensions.x / 2;

		view.setCenter(position);

		Window.setView(view);

		Window.draw(bImage);
		Window.draw(rect);
		Window.setView(Window.getDefaultView());
		Window.display();
		Window.clear();
	}
}
