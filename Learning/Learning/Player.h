#pragma once
#include<SFML/Graphics.hpp>
#include<SFML/Network.hpp>
#include "Server.h"

class Player
{
public:
	Player(int id, sf::TcpSocket* socket);
	~Player();
	sf::Vector2f position;
	int id;
	sf::TcpSocket* socket;
	
	sf::Packet* packet;

	void send(int id);
	void run(Server* server);

	void static _wrapper(Player* me, Server* server)
	{
		me->run(server);
	}

};

