#include<SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include<SFML/Network.hpp>
#include<iostream>
#include<string>
#include<map>
#include<conio.h>

#include "Server.h"


using namespace std;

int main()
{
	cout << "################################################" << endl;
	cout << "##             SIMPLE MMORG SERVER            ##" << endl;
	cout << "################################################ \n\n" << endl;
	cout << "> Server Listening..." << endl;

	Server* server = new Server();
	server->run();
}
