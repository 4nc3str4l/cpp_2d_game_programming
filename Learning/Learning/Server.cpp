#include<thread>
#include<iostream>
#include "Server.h"
#include "Player.h"

Server::Server()
{
	this->ip = new sf::IpAddress();
	this->socket = new sf::TcpListener();
	this->players = std::map<int, Player*>();
	this->it = std::map < int, Player* >::iterator();
	this->running = true;
	this->port = 2000;
	this->senderIP = new sf::IpAddress();
}


void Server::run()
{
	int id = 0;
	this->socket->listen(2000);
	sf::TcpSocket* sock = new sf::TcpSocket();
	std::cout << "Server Listening " << std::endl;
	while (running)
	{
		this->socket->accept(*sock);
		Player* player = new Player(id, sock);
		players.insert(std::pair<int, Player*>(++id, player));
		new std::thread(player->_wrapper, player, this);
		std::cout << "> New Player connected from: " << senderIP->toString() << " Num players online:" << players.size() << std::endl;
		id++;
	}
}

void Server::sendPlayerPos(sf::Packet pack)
{

	for (it = players.begin(); it != players.end(); it++)
	{
		it->second->socket->send(pack);
	}
	int inst, i2d;
	float x, y;
	
	//*packet >> inst >> i2d >> x >> y;
	//std::cout << "(" << x <<", " << y << ")" << "id " << i2d << " " << inst << std::endl;

}

Server::~Server()
{
}
