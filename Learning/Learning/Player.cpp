#include "Player.h"
#include <iostream>
#include <SFML/Network.hpp>



Player::Player(int id,sf::TcpSocket* socket)
{
	this->id = id;
	this->position = sf::Vector2f(0, 0);
	this->socket = socket;
}


Player::~Player()
{
}


void Player::send(int id)
{
	socket->send(*packet);
	packet->clear();
}

void Player::run(Server* server)
{
	sf::Packet pack;
	pack = sf::Packet();
	int packetHeader;

	pack << this->id;
	this->socket->send(pack);

	while (true)
	{
		this->socket->receive(pack);

		pack >> packetHeader;

		switch (packetHeader)
		{
		case 0x02:
			pack >> id;
			std::cout << id << std::endl;
			float x, y;
			pack >> x >> y;
			std::cout << "(" << x << ", " << y << ")" << std::endl;
			pack.clear();
			pack << 0x02 << id << x << y;
			server->sendPlayerPos(pack);
			break;
		default:
			break;
		}

	}
}