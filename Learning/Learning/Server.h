#pragma once
#include<SFML\Network.hpp>

class Player;

class Server
{
public:
	Server();
	~Server();
	void run();	
	sf::IpAddress* ip;
	sf::TcpListener *socket;
	sf::IpAddress* senderIP;
	std::map<int, Player*> players;
	std::map<int, Player*>::iterator it;


	unsigned short port;
	bool running;
	int packetHeader;

	void sendPlayerPos(sf::Packet pack);

};

